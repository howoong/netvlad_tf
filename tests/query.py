import netvlad_tf.nets as nets

import cv2
import numpy as np
import scipy.io as scio
import tensorflow.compat.v1 as tf
import time
import unittest
import os

# import netvlad_tf.net_from_mat as nfm

tf.disable_v2_behavior()

class TestNets(unittest.TestCase):
    def testVgg16NetvladPca(self):
        ''' Need example_stats.mat in matlab folder, which can be generated
        with get_example_stats.m. Also need translated checkpoint, can be
        generated with mat_to_checkpoint.py. '''
        tf.reset_default_graph()

        image_batch = tf.placeholder(dtype=tf.float32, shape=[None, None, None, 3])

        net_out = nets.vgg16NetvladPca(image_batch)
        saver = tf.train.Saver()

        sess = tf.Session()
        
        # Load trained parameters (checkpoint)
        saver.restore(sess, nets.defaultCheckpoint())
        # checkpoint_path = str(nets.defaultCheckpoint())
        # checkpoint_path = checkpoint_path[:checkpoint_path.rindex("/")]
        # print(checkpoint_path)
        # tf.train.write_graph(sess.graph.as_graph_def(), checkpoint_path,'graph.pb')

        # writer = tf.summary.FileWriter(checkpoint_path)
        # writer.add_graph(sess.graph)
        strDBPath = "/root/ShareFolder/netvlad_tf/featureDB/"
        strQueryPath = "/root/ShareFolder/netvlad_tf/query/"
        strQueryList = os.listdir(strQueryPath)

        for queryIdx in range(0, len(strQueryList)):
            print(str(queryIdx) + " Image Start")
            t = time.time()

            originImageFile = cv2.imread(strQueryPath + strQueryList[queryIdx])
            
            inim = cv2.resize(originImageFile, dsize=(480,640),interpolation=cv2.INTER_LINEAR)
            inim = cv2.cvtColor(inim, cv2.COLOR_BGR2RGB)

            batch = np.expand_dims(inim, axis=0)
            
            #%% Generate TF results
            # for _ in range(2):
            #     sess.run(net_out, feed_dict={image_batch: batch})
            result = sess.run(net_out, feed_dict={image_batch: batch})
            
            normDiff = []
            strDBList = os.listdir(strDBPath)
            for fileIndex in range(0, len(strDBList)):
                prevResult = np.load(strDBPath + strDBList[fileIndex])
            
                # for test - round()
                prevResult = np.round(prevResult, 5)
                result = np.round(result, 5)
                
                out_diff = np.abs(prevResult - result)
                normDiff.append(np.linalg.norm(out_diff))
            matchImageTop5 = []
            imgHeight, imgWidth, _ = originImageFile.shape
            top5normDiff = []
            print('Took %f seconds' % (time.time() - t))
            
            for i in range(0,5):
                minIdx = np.argmin(normDiff)
                matchImageTop5.append(strDBList[minIdx].replace('.npy',''))
                top5normDiff.append(normDiff[minIdx])
                normDiff[minIdx] *= 1000
            addImage = originImageFile
            
            for j in range(0,5):
                # matchImageFile = cv2.imread("/root/netvlad_tf/dataset/" + matchImageTop5[j])
                matchImageFile = cv2.imread("/root/ShareFolder/LeicaDB_Blur0/" + matchImageTop5[j])
                matchImageFile = cv2.resize(matchImageFile, dsize=(imgWidth, imgHeight), interpolation = cv2.INTER_LINEAR)
                location = (100, 300)
                font = cv2.FONT_HERSHEY_SIMPLEX
                fontScale = 10
                cv2.putText(matchImageFile, "Top" + str(j+1) + " Err:" + str(top5normDiff[j]), location, font, fontScale, (0,0,255), 10)
                cv2.putText(originImageFile, "Query Image", location, font, fontScale, (255,0,0), 10)
                addImage = cv2.hconcat([addImage, matchImageFile])
            
            cv2.imwrite("/root/ShareFolder/netvlad_tf/output/matchedResult" + str(queryIdx).zfill(3) + ".jpg", addImage)

if __name__ == '__main__':
    unittest.main()
