import netvlad_tf.nets as nets

import cv2
import numpy as np
import scipy.io as scio
import tensorflow.compat.v1 as tf
import time
import unittest
import os

# import netvlad_tf.net_from_mat as nfm

tf.disable_v2_behavior()

class TestNets(unittest.TestCase):
    def testVgg16NetvladPca(self):
        ''' Need example_stats.mat in matlab folder, which can be generated
        with get_example_stats.m. Also need translated checkpoint, can be
        generated with mat_to_checkpoint.py. '''
        tf.reset_default_graph()

        image_batch = tf.placeholder(dtype=tf.float32, shape=[None, None, None, 3])

        net_out = nets.vgg16NetvladPca(image_batch)
        saver = tf.train.Saver()
        sess = tf.Session()
        
        # Load trained parameters (checkpoint)
        saver.restore(sess, nets.defaultCheckpoint())
        
        # inim = cv2.imread(nfm.exampleImgPath())
        strResultPath = "/root/ShareFolder/netvlad_tf/featureDB_Blur0/"
        # strFilePath = "/root/netvlad_tf/dataset/"
        strFilePath = "/root/ShareFolder/LeicaDB/"
        strFileList = os.listdir(strFilePath)
        for fileIndex in range(0, len(strFileList)):
            t = time.time()
            imagePath = strFilePath + strFileList[fileIndex]
            inim = cv2.imread(imagePath)
            # imgHeight, imgWidth, _ = inim.shape
            # inim = cv2.resize(inim, dsize=(imgWidth,imgHeight),interpolation=cv2.INTER_LINEAR)
            inim = cv2.cvtColor(inim, cv2.COLOR_BGR2RGB)

            batch = np.expand_dims(inim, axis=0)

            #%% Generate TF results
            for _ in range(2):
                sess.run(net_out, feed_dict={image_batch: batch})
            # t = time.time()
            result = sess.run(net_out, feed_dict={image_batch: batch})
            print('Took %f seconds (%d/%d)' % ((time.time() - t), fileIndex, len(strFileList)))
            np.save(strResultPath + strFileList[fileIndex], result)

if __name__ == '__main__':
    unittest.main()
